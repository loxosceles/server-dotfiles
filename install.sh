#!/usr/bin/env sh

echo "Installer of required dependencies for server-dotfiles"

echo
echo "This is WIP, we are not automating anything here, we only print what you need"
echo "to install in order for this to work correctly."
echo
echo "Please issue the folllowing commands:"
echo
echo "\tgit clone git://github.com/jimeh/git-aware-prompt.git"
echo "\tsudo apt install pipenv"
echo "\tsudo apt install direnv"

